# завдання 1. Напиши програму, яка запитує у користувача два слова
# і виводить їх розділеними комою.

first_word = input("Input first word:")
second_word = input("Input second word:")
print(f"{first_word}, {second_word}")

# -----------------------------------------------------------------
# завдання 2. Напиши програму, яка запитує три цілих числа a, b, x
# та друкує їх добуток

a = input("Input first number:")
b = input("Input second number:")
x = input("Input third number:")
print(int(a) * int(b) * int(x))

# -----------------------------------------------------------------
# завдання 3
# Напишіть програму, яка розв'язує квадратне рівняння 𝑎𝑥2 + 𝑏𝑥 + 𝑐 = 0.
# Значення a, b та c вводяться з клавіатури. Для знаходження кореня
# використовуйте оператор зведення в ступінь,
# а не функцію math.sqrt, щоб отримати комплексні числа у випадку, якщо вираз під коренем негативний.

a = int(input("Input a:"))
b = int(input("Input b:"))
c = int(input("Input c:"))
d = b ** 2 - 4 * a * c
if d < 0:
    print("No solution")
else:
    x1 = (-b + d ** 0.5) / (2 * a)
    x2 = (-b - d ** 0.5) / (2 * a)
    print(f"x1 = {x1}")
    print(f"x2 = {x2}")
# ------------------------------------------------------------------
# Напишіть програму, в якій користувач вводить фразу с 10 символів.
# На екрані виведіть суму кодів символів

string = ""
while len(string) != 10:
    string = input("Input 10 characters:")
result = 0
for char in string:
    result += ord(char)
print(result)

# ------------------------------------------------------------------
# Завдання 5
# Напишіть програму, яка вводить з клавіатури текст і виводить його
# в оберненому порядку.

string = input("Input your messege: ")
revers_string = string[::-1]
print(revers_string)

# ------------------------------------------------------------------
# Завдання 6
# Напишіть програму, яка запитує користувача радіус круга і виводить
# його площу. Формула площі круга: S= 𝜋𝑟 2 .


import math


def is_number(radius: str) -> bool:
    try:
        float(radius)
        return True
    except ValueError:
        return False


radius = input("Input radius of the circle: ")
while not is_number(radius) or float(radius) < 0:
    radius = input("Input radius of the circle correct: ")
area_circle = float(radius) ** 2 * math.pi
print(f"S = {area_circle}")

