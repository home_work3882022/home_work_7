# Завдання 1
# Дано два рядки. Виведіть на екран символи, які є в обох рядках.

string_1 = "Hello world!!!"
string_2 = "World!"
crossing_string = ""

for char in string_1.lower():
    if string_2.lower().find(char) != -1 and crossing_string.find(char) == -1:
        crossing_string = crossing_string + char

print(crossing_string)

# ---------------------------------------------------------------
# Завдання 2
# Створіть програму, яка емулює роботу сервісу зі скорочення посилань.
# Повинна бути реалізована можливість введення початкового посилання та
# короткої назви і отримання початкового посилання за її назвою.

exit_value = "1"
links = dict()

while exit_value != "0":
    value = input("Enter your link:")
    key = input("Enter short name of the link:")
    links[key] = value
    exit_value = input("Enter 0 to exit or something else to continue:")

name = input("Enter search link name:")
print(links.get(name, "name is not found"))


# _______________________________________________________________________--
# Завдання 3
# Створіть програму, яка має 2 списки цілочисельних значень та друкує список
# унікальних значень без повтору, які є в 1 списку (немає в другому) і навпаки.

list_1 = [1, 2, 3, 4, 5]
list_2 = [1, 2, 3, 7, 8]
unique_1 = []
unique_2 = []
for item in list_1:
    if list_2.count(item):
        continue
    else:
        unique_1.append(item)
for item in list_2:
    if list_1.count(item):
        continue
    else:
        unique_2.append(item)
print(unique_1)
print(unique_2)

